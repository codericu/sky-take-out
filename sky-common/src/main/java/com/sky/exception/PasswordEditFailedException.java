package com.sky.exception;

public class PasswordEditFailedException extends BaseException{
    public PasswordEditFailedException() {
    }

    public PasswordEditFailedException(String message) {
        super(message);
    }
}
