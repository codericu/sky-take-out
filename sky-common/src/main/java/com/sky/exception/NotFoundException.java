package com.sky.exception;

public class NotFoundException extends BaseException{
    public NotFoundException(String message) {
        super(message);
    }
}
