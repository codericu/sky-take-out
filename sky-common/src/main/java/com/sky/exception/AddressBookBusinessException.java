package com.sky.exception;

public class AddressBookBusinessException extends BaseException{
    public AddressBookBusinessException() {
    }

    public AddressBookBusinessException(String message) {
        super(message);
    }
}
