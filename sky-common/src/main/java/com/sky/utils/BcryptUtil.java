package com.sky.utils;

import org.mindrot.jbcrypt.BCrypt;

public class BcryptUtil {

    // 产生一个随机盐值
    public static String generateSalt(int logRounds) {
        return BCrypt.gensalt(logRounds);
    }

    // 对密码进行bcrypt哈希加密（包括盐值）
    public static String hashPassword(String password, String salt) {
        return BCrypt.hashpw(password, salt);
    }

    // 验证密码（密码是加盐后的密码）
    public static boolean verifyPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }
}