package com.sky.constant;

public class JwtClaimsConstant {
    /**
     * jwt自定载荷名
     */
    public static final String EMP_ID="empId";
    public static final String USER_ID="userId";
    public static final String PHONE="phone";
    public static final String USERNAME="username";
    public static final String NAME="name";
}
