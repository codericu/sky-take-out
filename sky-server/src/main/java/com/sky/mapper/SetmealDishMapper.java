package com.sky.mapper;

import com.sky.annotaion.AutoFill;
import com.sky.entity.Dish;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {

    void insert(List<SetmealDish> setmealDishes);

    List<Long> selectByDishId(List<Long> ids);

    @Select("select id, setmeal_id, dish_id, name, price, copies from setmeal_dish where setmeal_id=#{id}")
    List<SetmealDish> selectBySetmealId(Long id);
    @Delete("delete from setmeal_dish where setmeal_dish.setmeal_id=#{id}")
    void deleteBySetmealId(Long id);

    void deleteBySetmealIds(List<Long> ids);
}
