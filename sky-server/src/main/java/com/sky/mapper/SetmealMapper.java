package com.sky.mapper;

import com.sky.annotaion.AutoFill;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.enumeration.OperationType;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 套餐管理
 */
@Mapper

public interface SetmealMapper {

     @Insert("insert into setmeal(category_id, name, price, description, image, create_time, update_time, create_user, update_user)" +
             "values(#{categoryId},#{name},#{price},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser})")
     @Options(useGeneratedKeys = true,keyProperty = "id")
     @AutoFill(OperationType.INSERT)
     void insert(Setmeal setmeal);

    @Select("select count(*) from setmeal where category_id=#{categoryId}")
    Integer selectByCategoryId(Long categoryId);


    List<Setmeal> selectSetmealByCategoryId(Long categoryId);


    List<SetmealVO> pageSelect(SetmealPageQueryDTO setmealPageQueryDTO);

    @Select("select setmeal.*,c.name categoryName from setmeal left join category c on setmeal.category_id = c.id where setmeal.id=#{id}")
    Setmeal selectBySetmealId(Long id);
    @AutoFill(OperationType.UPDATE)
    void update(Setmeal setmeal);

    void deleteById(List<Long> ids);


    @Select("select count(*) from setmeal where status=#{status};")
    Integer getSetmealNumberByStatus(Integer status);
}
