package com.sky.mapper;

import com.sky.annotaion.AutoFill;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;

import com.sky.vo.SetmealDishVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface DishMapper {
    @AutoFill(OperationType.INSERT)
    @Insert("insert into dish (name, category_id, price, image, description, status, create_time, update_time, create_user, update_user)" +
            "values (#{name},#{categoryId},#{price},#{image},#{description},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void insert(Dish dish);

    List<DishVO> pageSelect(Dish dish);

    void deleteById(List<Long> ids);

    @Select("select  d.* , category.name categoryName from dish d left join category on d.category_id = category.id where d.id=#{id} ")
    DishVO selectById(Long id);

    @AutoFill(OperationType.UPDATE)
    void update(Dish dish);
    @Select("select count(*) from dish where category_id=#{categoryId}")
    Integer selectByCategoryId(Long categoryId);



    @Select("select id, name, category_id, price, image, description, status, create_time, update_time, create_user, update_user from dish where category_id=#{categoryId}")
    List<Dish> selectDishByCategoryId(Long categoryId);

    @Select("select id, name, category_id, price, image, description, status, create_time, update_time, create_user, update_user from dish where category_id=#{categoryId}")
    List<DishVO> userSelectDishByCategoryId(Long categoryId);

    // @Select("select sd.name, sd.copies, d.image, d.description " +
    //            "from setmeal_dish sd left join dish d on sd.dish_id = d.id " +
    //            "where sd.setmeal_id = #{setmealId}")

    //@Select("select setmeal_dish.copies,dish.description,dish.image,dish.name from dish,setmeal_dish where dish.id=setmeal_dish.dish_id and setmeal_dish.setmeal_id=#{setmealId}")
    @Select("select sd.name, sd.copies, d.image, d.description " +
                      "from setmeal_dish sd left join dish d on sd.dish_id = d.id " +
                      "where sd.setmeal_id = #{setmealId}")
    List<SetmealDishVO> selectDishBySermealId(Long setmealId);

    @Select("select count(*) from dish where status=#{status};")
    Integer getDishNumberByStatus(Integer status);
}
