package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorsMapper {

    void insertBatch(List<DishFlavor> flavors);

    @Select("select  id, dish_id, name, value from dish_flavor where dish_id=#{dishId}")
    List<DishFlavor> selectById(Long dishId);


    @Delete("delete from dish_flavor where dish_id=#{dishId}")
    void delete(Long dishId);
}
