package com.sky.mapper;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {


    List<ShoppingCart> selectByCondition(ShoppingCart shoppingCartCondition);

    void update(ShoppingCart shoppingCart);


    void insert(ShoppingCart shoppingCart);



    void deleteAll(Long currentId);

    void deleteByShoppingCart(ShoppingCart shoppingCart);

    @Select("select id, name, image, user_id, dish_id, setmeal_id, dish_flavor, number, amount, create_time from shopping_cart where user_id=#{userId}")
    List<ShoppingCart> selectByUserId(Long currentId);
}
