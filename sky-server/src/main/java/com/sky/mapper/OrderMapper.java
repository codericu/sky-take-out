package com.sky.mapper;

import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;

@Mapper
public interface OrderMapper {
    void insert(Orders orders);

    /**
     * 根据订单号和用户id查询订单
     *
     * @param orderNumber
     * @param userId
     */
    @Select("select * from orders where number = #{orderNumber} and user_id= #{userId}")
    Orders getByNumberAndUserId(String orderNumber, Long userId);

    /**
     * 修改订单信息
     *
     * @param orders
     */
    void update(Orders orders);

    List<OrderVO> list(OrdersPageQueryDTO ordersPageQueryDTO);

    @Select("select id, number, status, user_id, address_book_id, order_time, checkout_time, pay_method, pay_status, amount, remark, phone, address, user_name, consignee, cancel_reason, rejection_reason, cancel_time, estimated_delivery_time, delivery_status, delivery_time, pack_amount, tableware_number, tableware_status from orders where id=#{id}")
    Orders getById(Long id);


    Integer getNumberByStatus(Integer status, LocalDate date);


    List<Orders> findByStatus(Integer status, LocalDateTime orderTime, LocalDateTime now);

    @Select("select * from orders where status=#{status} and order_time < #{time}")
    List<Orders> selectByStatus(Integer status, LocalDateTime time);

    /*获取某天营业额*/
    @Select("select sum(amount) from orders where status=#{status} and date(order_time) = #{date}")
    Double getTurnover(Integer status, LocalDate date);
    /*获取最近30天营业额*/
    @Select("select sum(amount) from orders where status=#{status} and date(order_time) between #{begin} and #{end}")
    Double getRangeTurnover(Integer status, LocalDate begin,LocalDate end);


    @Select("select count(*) from orders where date(order_time) <= #{date}")
    Integer getTodayAllNumber(LocalDate date);

    @Select("select od.name name, sum(od.number) number from orders,order_detail od where orders.id = od.order_id and orders.status=5 and date(order_time) between #{begin} and #{end} group by od.name order by number desc limit 10")
    List<GoodsSalesDTO> getTopTen(LocalDate begin, LocalDate end);

    Integer getTodayNumberByStatus(Integer status,LocalDate date);

    Integer getRangeNumberByStatus(Integer status,LocalDate begin, LocalDate end);
}
