package com.sky.mapper;

import com.sky.entity.User;
import com.sky.vo.UserReportVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;

@Mapper
public interface UserMapper {




    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time)" +
            "values (#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void insertUser(User firstUser);
    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where openid=#{openid}")
    User selectUserByOpenid(String openid);

    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where id=#{userId}")
    User getById(Long userId);

    @Select("select count(*) from user where date(create_time) = #{date}")
    Integer getNewUserNumber(LocalDate date);
    @Select("select count(*) from user where date(create_time) <= #{date}")
    Integer getAllUserNumber(LocalDate date);


    @Select("select count(*) from user where date(create_time) between #{begin} and #{end}")
    Integer getRangeNewUserNumber(LocalDate begin,LocalDate end);

}
