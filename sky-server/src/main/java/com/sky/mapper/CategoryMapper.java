package com.sky.mapper;

import com.sky.annotaion.AutoFill;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper {
    @Insert("insert into category (type, name, sort, status, create_time, update_time, create_user, update_user)" +
            "values (#{type},#{name},#{sort},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    @AutoFill(value = OperationType.INSERT)
    void insert(Category category);

    List<Category> pageSelect(Category category);

    @Delete("delete from category where id=#{id}")
    void delete(Long id);

    @Select("select id, type, name, sort, status, create_time, update_time, create_user, update_user from category where id=#{id}")
    Category select(Long id);
    @AutoFill(value = OperationType.UPDATE)
    void update(Category category);

    Category[] selectByType(Integer type);
}
