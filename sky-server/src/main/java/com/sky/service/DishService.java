package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import com.sky.vo.SetmealDishVO;

import java.util.List;

public interface DishService {
    void save(DishDTO dishDTO);

    PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO);

    void remove(List<Long> ids);

    DishVO findById(Long id);

    void change(DishDTO dishDTO);

    void changeStatus(Integer status, Long id);

    List<Dish> findByCategoryId(Long categoryId);

    List<DishVO> userFindByCategoryId(Long categoryId);

    List<SetmealDishVO> getDishBySetmealId(Long id);
}
