package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;

public interface CategoryService {
    void save(CategoryDTO categoryDTO);

    PageResult pageQuery(CategoryPageQueryDTO categoryPageQueryDTO);

    void remove(Long id);

    Category findById(Long id);

    void change(CategoryDTO categoryDTO);

    void statusChange(Integer status, Long id);

    Category[] findByType(Integer type);
}
