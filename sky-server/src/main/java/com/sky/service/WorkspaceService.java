package com.sky.service;

import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;

import java.time.LocalDate;

public interface WorkspaceService {

    /**
     *  查询某日运营数据
     * @return
     */
    BusinessDataVO businessDate(LocalDate date);
    /**
     *  查询最近30天运营数据
     * @return
     */
    BusinessDataVO getBusinessDate(LocalDate begin,LocalDate end);
    /**
     *  查询套餐总览
     * @return
     */
    SetmealOverViewVO overviewSetmeals();
    /**
     *  查询菜品总览
     * @return
     */
    DishOverViewVO overviewDish();
    /**
     *  查询订单管理数据
     * @return
     */
    OrderOverViewVO overviewOrder();

}
