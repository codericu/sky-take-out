package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.annotaion.AutoFill;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BcryptUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {


    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();
        //


        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        // TODO 后期需要进行md5加密，然后再进行比对

        //String userPassword = DigestUtils.md5DigestAsHex(password.getBytes());

        if (!BcryptUtil.verifyPassword(password, employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
//        if (!userPassword.equals(employee.getPassword())) {
//            //密码错误
//            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
//        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    /**
     * 添加员工
     *
     * @param employeeDTO
     */
    @Override
    public void save(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();

        //对象属性拷贝
        BeanUtils.copyProperties(employeeDTO, employee);

        //设置账号的状态，默认正常状态 1表示正常，0表示锁定
        employee.setStatus(StatusConstant.ENABLE);

        //设置密码，默认密码123456

        //employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));

        String s = BcryptUtil.generateSalt(10);
        employee.setPassword(BcryptUtil.hashPassword(PasswordConstant.DEFAULT_PASSWORD,s));

        //设置创建时间和修改时间
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//        //记录创建人id和修改人id
//
//
//        employee.setCreateUser(BaseContext.getCurrentId());//后期修改
//        employee.setUpdateUser(BaseContext.getCurrentId());
        //调用mapper方法保存
        employeeMapper.insert(employee);
    }

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuery(EmployeePageQueryDTO employeePageQueryDTO) {
        //1.设置分页参数
        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());
        //2.设置查询条件
        List<Employee> employees=employeeMapper.pageSelect(employeePageQueryDTO.getName());
        //3.把查询结果封装为PageResult对象
        Page<Employee> pageHelper= (Page<Employee>) employees;
        return new  PageResult(pageHelper.getTotal(),pageHelper.getResult());
    }

    /**
     * 启用禁用员工账号
     * @param status
     */
    @Override
    public void statusChange(Integer status,Long id) {
        Employee employee = new Employee();
        employee.setStatus(status);
        employee.setId(id);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.update(employee);
    }

    /**
     * 通过id查询员工信息
     * @param id
     * @return
     */
    @Override
    public Employee findById(Long id) {
        Employee employee=employeeMapper.selectById(id);
        employee.setPassword("****");
        return employee;
    }

    /**
     * 编辑员工信息
     * @param employeeDTO
     */
    @Override
    public void change(EmployeeDTO employeeDTO) {
        Employee employee=new Employee();
        //对象拷贝
        BeanUtils.copyProperties(employeeDTO,employee);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.update(employee);
    }

    /**
     * 修改员工密码
     * @param passwordEditDTO
     */
    @Override
    public void editPassword(PasswordEditDTO passwordEditDTO) {
        //校验员工输入旧密码是否正确
        Employee employee=employeeMapper.selectById(BaseContext.getCurrentId());
        String oldPassword = employee.getPassword();
        //生成盐值
        String salt = BcryptUtil.generateSalt(10);
        if (BcryptUtil.verifyPassword(passwordEditDTO.getOldPassword(),oldPassword)){
            //旧密码不一致，抛出异常
        throw new PasswordErrorException(MessageConstant.PASSWORD_EDIT_FAILED);
        }
        //旧密码一致，修改密码
        employee.setPassword(BcryptUtil.hashPassword(passwordEditDTO.getNewPassword(),salt));
        //调用持久层
        employeeMapper.update(employee);

    }
}