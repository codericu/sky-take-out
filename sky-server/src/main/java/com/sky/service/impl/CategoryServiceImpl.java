package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.annotaion.AutoFill;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    DishMapper dishMapper;
    @Autowired
    SetmealMapper setmealMapper;

    /**
     * 添加分类
     * @param categoryDTO
     */
    @Override
    public void save(CategoryDTO categoryDTO) {
        Category category=new Category();
        //对象拷贝
        BeanUtils.copyProperties(categoryDTO,category);
        //设置分类状态
        category.setStatus(StatusConstant.DISABLE);
        //设置创建时间和更新时间
//        category.setCreateTime(LocalDateTime.now());
//        category.setUpdateTime(LocalDateTime.now());
//        //设置创建人和修改人
//        category.setCreateUser(BaseContext.getCurrentId());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.insert(category);
    }

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     */
    @Override
    public PageResult pageQuery(CategoryPageQueryDTO categoryPageQueryDTO) {
        Category category = Category.builder().name(categoryPageQueryDTO.getName()).type(categoryPageQueryDTO.getType()).build();
        //开启分页参数
        PageHelper.startPage(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());
        //设置查询条件
        Page<Category> page = (Page<Category>) categoryMapper.pageSelect(category);
        //获取结果
        return new  PageResult(page.getTotal(),page.getResult());

    }

    /**
     * 通过id删除分类信息
     * @param id
     */
    @Override
    public void remove(Long id) {
        //1.查询菜品表：查询当前删除的分类下是否存在菜品
        Integer dishCount = dishMapper.selectByCategoryId(id);
        if (dishCount>0){
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        //2.查询套餐表：查询当前删除的分类下是否存在套餐
        Integer setmealCount=setmealMapper.selectByCategoryId(id);
        if (setmealCount>0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        categoryMapper.delete(id);
    }

    /**
     * 通过id查询分类
     * @param id
     * @return
     */
    @Override
    public Category findById(Long id) {
        Category category=categoryMapper.select(id);
        return category;
    }

    /**
     * 修改分类信息
     * @param categoryDTO
     */
    @Override
    public void change(CategoryDTO categoryDTO) {
        Category category = new Category();
        //对象拷贝
        BeanUtils.copyProperties(categoryDTO,category);
        //更新修改时间和修改人
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.update(category);
    }

    /**
     * 修改分类状态
     * @param status
     * @param id
     */
    @Override
    public void statusChange(Integer status, Long id) {
        Category category = new Category();
        category.setId(id);
        category.setStatus(status);
        categoryMapper.update(category);
    }

    /**
     * 通过类型查询分类信息
     * @param type
     * @return
     */
    @Override
    public Category[] findByType(Integer type) {
         Category[] categories=categoryMapper.selectByType(type);
        return categories;
    }


}
