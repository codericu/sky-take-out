package com.sky.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.NotFoundException;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Consumer;

@Service
@Slf4j
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    SetmealMapper setmealMapper;
    @Autowired
    SetmealDishMapper setmealDishMapper;



    /**
     * 添加套餐
     * @param setmealDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SetmealDTO setmealDTO) {

        Setmeal setmeal = new Setmeal();
        //对象拷贝
        BeanUtils.copyProperties(setmealDTO,setmeal);
        //添加套餐状态
        setmeal.setStatus(StatusConstant.DISABLE);
        //加入套餐
        setmealMapper.insert(setmeal);
        //获取套餐id
        Long setmealId = setmeal.getId();
        //更改菜品所属套餐
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealId);

        });
        //维护菜品和套餐关系表
        setmealDishMapper.insert(setmealDishes);

    }

    /**
     * 分页查询
     * @param setmealPageQueryDTO
     */
    @Override
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {

        //1.开启分页查询
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        //2.基础条件查询
        Page<SetmealVO> page =(Page<SetmealVO>)setmealMapper.pageSelect(setmealPageQueryDTO);
        //3.封装查询结果
        long total = page.getTotal();
        List result = page.getResult();
        log.info("{},{}", total, result);
        //4.判断搜索结果是否为空
        if (!(result.size()>0)){
            throw new NotFoundException(MessageConstant.SETMEAL_NOT_FOUND);
        }
        return new PageResult(total,result);


    }

    /**
     * 管理端根据id查询套餐
     * @param id
     * @return
     */
    @Override
    public SetmealVO findById(Long id) {
        //1.查询套餐表
        Setmeal setmeal=setmealMapper.selectBySetmealId(id);
        //2.查询套餐和菜品关系表
       List<SetmealDish> dishList =setmealDishMapper.selectBySetmealId(id);
       //3.将结果转换为vo对象
        SetmealVO setmealVO = new SetmealVO();

        BeanUtils.copyProperties(setmeal,setmealVO);
        setmealVO.setSetmealDishes(dishList);
        return setmealVO;
    }
    /**
     * 用户端根据id查询套餐
     * @param categoryId
     * @return
     */
    @Override
    public List<Setmeal> userFindById(Long categoryId) {
        List<Setmeal> setmeal=setmealMapper.selectSetmealByCategoryId(categoryId);
        return setmeal;
    }

    /**
     * 跟新套餐
     * @param setmealDTO
     */
    @Override
    @Transactional
    public void changeSetmeal(SetmealDTO setmealDTO) {
        //把DTO对象转为entity对象
        Setmeal setmeal = new Setmeal();
        //对象拷贝
        BeanUtils.copyProperties(setmealDTO,setmeal);
        //跟新套餐信息
        setmealMapper.update(setmeal);
        //删除原始菜品
        setmealDishMapper.deleteBySetmealId(setmealDTO.getId());
        //获取setmeal主键
        Long id = setmealDTO.getId();
        //添加菜品
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (setmealDishes!=null && setmealDishes.size()>0){
            setmealDishes.forEach(setmealDish -> {
                setmealDish.setSetmealId(id);
            });
            setmealDishMapper.insert(setmealDishes);

        }
    }

    /**
     * 套餐起售/停售
     * @param status
     * @param id
     */
    @Override
    public void changeStatus(Integer status, Long id) {
        Setmeal setmeal = new Setmeal();
        setmeal.setId(id);
        setmeal.setStatus(status);
        setmealMapper.update(setmeal);
    }

    /**
     * 删除套餐
     * @param ids
     */
    @Override
    @Transactional
    public void remove(List<Long> ids) {
        //1.判断套餐是否起售
        ids.forEach(id -> {
            Setmeal setmeal = setmealMapper.selectBySetmealId(id);
            if (setmeal.getStatus()==(StatusConstant.ENABLE)){
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        });
        //2.删除套餐
        setmealMapper.deleteById(ids);
        //3.删除菜品
        setmealDishMapper.deleteBySetmealIds(ids);

    }



}
