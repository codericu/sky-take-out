package com.sky.service.impl;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import org.apache.ibatis.annotations.Select;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ReportServiceImp implements ReportService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    WorkspaceService workspaceService;


    /**
     * 营业额统计接口
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {
        StringJoiner dateList = new StringJoiner(",");
        StringJoiner turnoverList = new StringJoiner(",");


        // 获取时间范围内的所有时间值
        List<LocalDate> allTimes = getAllTimesInRange(begin, end);
        for (LocalDate date : allTimes) {
            Double turnover = orderMapper.getTurnover(Orders.COMPLETED, date);
            if (turnover != null) {
                dateList.add(date.toString());
                turnoverList.add(turnover.toString());
            }
        }

        return new TurnoverReportVO(dateList.toString(), turnoverList.toString());
    }

    /**
     * 营业额统计接口
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        StringJoiner dateList = new StringJoiner(",");
        StringJoiner totalUserList = new StringJoiner(",");
        StringJoiner newUserList = new StringJoiner(",");
        List<LocalDate> allTimesInRange = getAllTimesInRange(begin, end);
        for (LocalDate date : allTimesInRange) {
            //查询某天用户总量
            Integer allUserNumber = userMapper.getAllUserNumber(date);
            //查询某天新增用户量
            Integer newUserNumber = userMapper.getNewUserNumber(date);
            if (allUserNumber != null && newUserNumber != null) {
                dateList.add(date.toString());
                totalUserList.add(allUserNumber.toString());
                newUserList.add(newUserNumber.toString());

            }

        }
        return new UserReportVO(dateList.toString(), totalUserList.toString(), newUserList.toString());
    }

    /**
     * 订单统计
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {
        //1.日期
        StringJoiner dateList = new StringJoiner(",");
        //2.每日订单数
        StringJoiner orderCountList = new StringJoiner(",");
        //3.每日有效订单数
        StringJoiner validOrderCountList = new StringJoiner(",");
        //获取所有日期
        List<LocalDate> allTimesInRange = getAllTimesInRange(begin, end);
        for (LocalDate date : allTimesInRange) {

            //每日订单数
            Integer allNumber = orderMapper.getTodayAllNumber(date);
            //有效订单数
            Integer numberByStatus = orderMapper.getNumberByStatus(Orders.COMPLETED, date);
            if (allNumber != null && numberByStatus != null) {
                dateList.add(date.toString());//日期
                orderCountList.add(allNumber.toString());//每日订单数
                validOrderCountList.add(numberByStatus.toString());//有效订单数
            }

        }
        //4.订单总数
        Integer allNumber = orderMapper.getTodayAllNumber(LocalDate.now());
        //5.有效订单数
        Integer numberByStatus = orderMapper.getNumberByStatus(Orders.COMPLETED, null);
        //6.订单完成率
        double v = 0.0;
        if (allNumber != 0) {
            v = (double) numberByStatus / (double) allNumber;
        }
        return new OrderReportVO(dateList.toString(), orderCountList.toString(), validOrderCountList.toString(), allNumber, numberByStatus, v);
    }

    /**
     * 销量排名前十
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO topTen(LocalDate begin, LocalDate end) {
        List<GoodsSalesDTO> goodsSalesDTO = orderMapper.getTopTen(begin, end);
        StringJoiner nameList = new StringJoiner(",");
        StringJoiner numberList = new StringJoiner(",");
        if (goodsSalesDTO != null && goodsSalesDTO.size() > 0) {
            for (GoodsSalesDTO salesDTO : goodsSalesDTO) {
                nameList.add(salesDTO.getName());
                numberList.add(salesDTO.getNumber().toString());
            }
        }

        return new SalesTop10ReportVO(nameList.toString(), numberList.toString());
    }

    /**
     * 导出excel数据
     */
    @Override
    public void exportDate(OutputStream outputStream) {
        //1.查询最近30天数据
        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now().minusDays(1);
        BusinessDataVO businessDataVO = workspaceService.getBusinessDate(begin, end);
        //2.使用poi把数据导入excel中
        //2.1读入模板文件
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("templet/运营数据报表模板.xlsx");
        try {
            XSSFWorkbook excel = new XSSFWorkbook(inputStream);
            //2.2放入数据
            XSSFSheet sheet = excel.getSheetAt(0);

            sheet.getRow(1).getCell(1).setCellValue("时间：" + begin + " 到 " + end);

            sheet.getRow(3).getCell(2).setCellValue(businessDataVO.getTurnover());
            sheet.getRow(3).getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
            sheet.getRow(3).getCell(6).setCellValue(businessDataVO.getNewUsers());
            sheet.getRow(4).getCell(2).setCellValue(businessDataVO.getValidOrderCount());
            sheet.getRow(4).getCell(4).setCellValue(businessDataVO.getUnitPrice());
            //放入明细数据
            for (int i = 0; i < 30; i++) {

                LocalDate newDate = begin.plusDays(i);
                BusinessDataVO businessDataVO1 = workspaceService.businessDate(newDate);
                sheet.getRow(7 + i).getCell(1).setCellValue(newDate.toString());
                Double turnover= businessDataVO1.getTurnover();
                if (turnover==null){
                    sheet.getRow(7 + i).getCell(2).setCellValue(0);
                }else {
                    sheet.getRow(7 + i).getCell(2).setCellValue(turnover);
                }
                Integer validOrderCount= businessDataVO1.getValidOrderCount();
                if (validOrderCount==null){

                    sheet.getRow(7 + i).getCell(3).setCellValue(0);
                }else {

                    sheet.getRow(7 + i).getCell(3).setCellValue(validOrderCount);
                }
                Double orderCompletionRate= businessDataVO1.getOrderCompletionRate();
                if (orderCompletionRate==null){

                    sheet.getRow(7 + i).getCell(4).setCellValue(0);
                }else {

                    sheet.getRow(7 + i).getCell(4).setCellValue(orderCompletionRate);
                }
                Double unitPrice= businessDataVO1.getUnitPrice();
                if (unitPrice==null){

                    sheet.getRow(7 + i).getCell(5).setCellValue(0);
                }else {
                    sheet.getRow(7 + i).getCell(5).setCellValue(unitPrice);

                }
                Integer newUsers= businessDataVO1.getNewUsers();
                if (newUsers==null){

                    sheet.getRow(7 + i).getCell(6).setCellValue(0);
                }else {
                    sheet.getRow(7 + i).getCell(6).setCellValue(newUsers);

                }
            }
            //3.使用输出流导出数据
            excel.write(outputStream);
            //关闭资源
            outputStream.close();
            excel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //获取时间范围中的所有值
    private static List<LocalDate> getAllTimesInRange(LocalDate start, LocalDate end) {
        List<LocalDate> allTimes = new ArrayList<>();
        LocalDate current = start;

        while (current.isBefore(end) || current.isEqual(end)) {
            allTimes.add(current);
            current = current.plus(1, ChronoUnit.DAYS); // 这里可以根据需要调整递增的时间单位
        }

        return allTimes;
    }
}
