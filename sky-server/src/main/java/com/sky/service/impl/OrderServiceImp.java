package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.*;
import com.sky.entity.*;
import com.sky.exception.AddressBookBusinessException;
import com.sky.exception.BaseException;
import com.sky.exception.OrderBusinessException;
import com.sky.exception.ShoppingCartBusinessException;
import com.sky.mapper.*;

import com.sky.mysocket.WebSocketServer;
import com.sky.result.PageResult;
import com.sky.service.OrderService;
import com.sky.utils.WeChatPayUtil;
import com.sky.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Slf4j
public class OrderServiceImp implements OrderService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    AddressBookMapper addressBookMapper;
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    OrderDetailMapper orderDetailMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    WeChatPayUtil weChatPayUtil;
    @Autowired
    WebSocketServer webSocketServer;

    /**
     * 历史订单查询
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageResult(OrdersPageQueryDTO ordersPageQueryDTO) {
        //1.使用pageHelper插件
        PageHelper.startPage(ordersPageQueryDTO.getPage(), ordersPageQueryDTO.getPageSize());
        //2.设置查询条件
        List<OrderVO> list = orderMapper.list(ordersPageQueryDTO);
        log.info("订单数据表{}", list);
        //3.查询订单详情表
        list.forEach(orderVO -> {
            //4.拼接订单中菜品名称
            StringBuilder stringBuilder = new StringBuilder();
            //5.查询每个订单中的菜品
            List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(orderVO.getId());
            orderDetailList.forEach(orderDetail -> {
                stringBuilder.append(orderDetail.getName());
            });
            orderVO.setOrderDishes(stringBuilder.toString());
            orderVO.setOrderDetailList(orderDetailList);
            log.info("订单详情表数据{}", orderDetailList);
        });

        Page<OrderVO> page = (Page<OrderVO>) list;

        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 提交订单
     *
     * @param ordersSubmitDTO
     * @return
     */
    @Override
    public OrderSubmitVO submitOrder(OrdersSubmitDTO ordersSubmitDTO) {
        //判断地址是否为空
        AddressBook addressBook = addressBookMapper.getById(ordersSubmitDTO.getAddressBookId());
        if (addressBook == null) {
            throw new AddressBookBusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }
        //判断购物车是否存在
        Long userId = BaseContext.getCurrentId();
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByUserId(userId);
        if (shoppingCartList == null || shoppingCartList.size() == 0) {
            throw new ShoppingCartBusinessException(MessageConstant.SHOPPING_CART_IS_NULL);
        }

        //1.对象拷贝
        Orders orders = new Orders();
        BeanUtils.copyProperties(ordersSubmitDTO, orders);
        //2.补充缺少数据
        orders.setUserId(userId);//用户id
        orders.setStatus(Orders.PENDING_PAYMENT);//订单状态
        orders.setNumber(UUID.randomUUID().toString());//订单号
        orders.setOrderTime(LocalDateTime.now());//下单时间
        orders.setPhone(addressBook.getPhone());//收件人手机号
        orders.setConsignee(addressBook.getConsignee());//收件人姓名
        orders.setAddress(addressBook.getDetail());//具体收件地址
        orders.setPayStatus(Orders.UN_PAID);//支付状态
        //3.插入订单表数据
        orderMapper.insert(orders);

        //4.订单明细表数据
        ArrayList<OrderDetail> orderDetailList = new ArrayList<>();
        for (ShoppingCart cart : shoppingCartList) {
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(cart, orderDetail);
            orderDetail.setOrderId(orders.getId());
            orderDetailList.add(orderDetail);
        }
        //5.像明细表插入n条数据
        orderDetailMapper.insert(orderDetailList);
        //6.清空购物车
        shoppingCartMapper.deleteAll(userId);
        //7.封装返回结果
        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder()
                .id(orders.getId())//订单id
                .orderAmount(orders.getAmount())//订单金额
                .orderNumber(orders.getNumber())//订单号
                .orderTime(orders.getOrderTime())//下单时间
                .build();


        return orderSubmitVO;
    }

    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    @Override
    public OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        //1.获取当前用户登录id
        Long userId = BaseContext.getCurrentId();
        User user = userMapper.getById(userId);
        //2.调用微信接口，生成预支付交易单
        JSONObject jsonObject = weChatPayUtil.pay(
                ordersPaymentDTO.getOrderNumber(),//获取商户订单号
                new BigDecimal(0.01),
                "苍穹外卖订单",
                user.getOpenid()//微信用户的openid
        );
        //3.判断订单状态
        if (jsonObject.getString("code") != null && jsonObject.getString("code").equals("ORDERPAID")) {
            throw new OrderBusinessException("该订单已支付");
        }

        OrderPaymentVO vo = jsonObject.toJavaObject(OrderPaymentVO.class);
        vo.setPackageStr(jsonObject.getString("package"));
        return vo;
    }

    /**
     * 支付成功，修改订单状态
     *
     * @param outTradeNo
     */
    public void paySuccess(String outTradeNo) {
        // 当前登录用户id
        Long userId = BaseContext.getCurrentId();

        // 根据订单号查询当前用户的订单
        Orders ordersDB = orderMapper.getByNumberAndUserId(outTradeNo, userId);

        // 根据订单id更新订单的状态、支付方式、支付状态、结账时间
        Orders orders = Orders.builder()
                .id(ordersDB.getId())
                .status(Orders.TO_BE_CONFIRMED)
                .payStatus(Orders.PAID)
                .checkoutTime(LocalDateTime.now())
                .build();

        orderMapper.update(orders);
        //////////////////////////////////////////////
        Map map = new HashMap();
        map.put("type", 1);//消息类型，1表示来单提醒
        map.put("orderId", orders.getId());
        map.put("content", "订单号：" + outTradeNo);

        //通过WebSocket实现来单提醒，向客户端浏览器推送消息
        webSocketServer.sendToAllClient(JSON.toJSONString(map));
        ///////////////////////////////////////////////////
    }

    /**
     * 再来一单
     *
     * @param id
     */
    @Override
    public void repetitionPayment(Long id) throws Exception {
        //1.查询订单表
        Orders orders = orderMapper.getById(id);


        //2.补充缺少数据
        orders.setStatus(Orders.PENDING_PAYMENT);//订单状态
        orders.setNumber(UUID.randomUUID().toString());//订单号
        orders.setOrderTime(LocalDateTime.now());//下单时间
        orders.setPayStatus(Orders.UN_PAID);//支付状态
        //3.插入订单表数据
        orderMapper.insert(orders);
        //4.查询订单详情表
        List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(id);
        //5.插入详情表
        orderDetailList.forEach(orderDetail -> {
            orderDetail.setOrderId(orders.getId());
        });
        orderDetailMapper.insert(orderDetailList);

        //1.获取当前用户登录id
        Long userId = BaseContext.getCurrentId();
        User user = userMapper.getById(userId);
        //2.调用微信接口，生成预支付交易单
        JSONObject jsonObject = weChatPayUtil.pay(
                orders.getNumber(),//获取商户订单号
                new BigDecimal(0.01),
                "苍穹外卖订单",
                user.getOpenid()//微信用户的openid
        );
        //3.判断订单状态
        if (jsonObject.getString("code") != null && jsonObject.getString("code").equals("ORDERPAID")) {
            throw new OrderBusinessException("该订单已支付");
        }


    }

    /**
     * 查询订单详情
     *
     * @param id
     * @return
     */
    @Override
    public OrderVO orderDetailById(Long id) {
        OrderVO orderVO = new OrderVO();
        //1.查询订单信息
        Orders orders = orderMapper.getById(id);
        BeanUtils.copyProperties(orders, orderVO);
        //2.查询订单详情表
        List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(id);
        orderVO.setOrderDetailList(orderDetailList);


        return orderVO;
    }

    /**
     * 取消订单
     *
     * @param ordersCancelDTO
     */
    @Override
    public void cancel(OrdersCancelDTO ordersCancelDTO) {
        //1.查询订单表
        Orders orders = orderMapper.getById(ordersCancelDTO.getId());
        //2.设置用户id
        orders.setUserId(BaseContext.getCurrentId());

        BeanUtils.copyProperties(ordersCancelDTO, orders);
        if (orders.getPayStatus() == 1) {
            orders.setPayStatus(3);
            throw new BaseException("正在进行退款");
        }
        orders.setCancelTime(LocalDateTime.now());
        orders.setStatus(6);
        //5.修改数据表
        orderMapper.update(orders);
    }

    /**
     * 统计各个订单状态数量
     *
     * @return
     */
    @Override
    public OrderStatisticsVO orderCount() {
        //1.统计待接单数量
        Integer toBeConfirmedCount = orderMapper.getNumberByStatus(Orders.REFUND,null);
        //2.统计带派送数量
        Integer confirmedCount = orderMapper.getNumberByStatus(Orders.CONFIRMED,null);
        //3.统计派送中数量
        Integer deliverInProgressCount = orderMapper.getNumberByStatus(Orders.DELIVERY_IN_PROGRESS,null);

        //4.封装为vo对象返回
        OrderStatisticsVO orderStatisticsVO = new OrderStatisticsVO();
        orderStatisticsVO.setToBeConfirmed(toBeConfirmedCount);
        orderStatisticsVO.setConfirmed(confirmedCount);
        orderStatisticsVO.setDeliveryInProgress(deliverInProgressCount);
        return orderStatisticsVO;
    }


    /**
     * 拒单
     *
     * @param ordersRejectionDTO
     */
    @Override
    public void rejectionOrder(OrdersRejectionDTO ordersRejectionDTO) {
        Orders orders = new Orders();
        //1.获取员工id
        Long id = ordersRejectionDTO.getId();
        //2.查询用户订单状态，是否为已支付，
        Orders userOrders = orderMapper.getById(id);
        if (userOrders.getPayStatus() == 1) {
            //订单状态为已支付，将进行退款操作
            throw new BaseException("正在进行退款操作");
        }
        //3.修改用户订单状态
        orders.setId(id);
        orders.setStatus(6);
        orderMapper.update(orders);

    }

    /**
     * 接单
     *
     * @param ordersConfirmDTO
     */
    @Override
    public void confirmOrder(OrdersConfirmDTO ordersConfirmDTO) {

        Orders orders = new Orders();
        orders.setId(ordersConfirmDTO.getId());
        orders.setStatus(3);
        orderMapper.update(orders);

    }

    /**
     * 派送订单
     *
     * @param id
     * @return
     */
    @Override
    public void deliveryOrder(Long id) {
        Orders orders = new Orders();
        orders.setId(id);
        orders.setStatus(4);
        orderMapper.update(orders);
    }

    /**
     * 完成订单
     *
     * @param id
     */
    @Override
    public void complete(Long id) {
        Orders orders = new Orders();
        orders.setId(id);
        orders.setStatus(5);
        orderMapper.update(orders);
    }

    /**
     * 用户催单
     *
     * @param id
     */
    @Override
    public void reminder(Long id) {
        //1.查询订单
        Orders orders = orderMapper.getById(id);
        //2.
        Map message = new HashMap<>();
        message.put("type", 2);//2.代表用户催单
        message.put("orderId", id);
        message.put("content", "用户id为：" + orders.getUserId() + "订单号：" + orders.getNumber());
        webSocketServer.sendToAllClient(JSON.toJSONString(message));
    }


}
