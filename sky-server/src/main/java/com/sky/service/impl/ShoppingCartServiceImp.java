package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class ShoppingCartServiceImp implements ShoppingCartService {


    @Autowired
    ShoppingCartMapper shoppingCartMapper;

    @Autowired
    DishMapper dishMapper;

    @Autowired
    SetmealMapper setmealMapper;


    /**
     * 添加购物车方法业务，具体实现
     * @param shoppingCartDTO
     */
    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {
    //1.判断当前待添加的商品是否才能在（菜品/套餐）：
        //实现：使用动态SQL查询购物车中的商品是否存在
        //条件 ishId  setmealId dishFlavor
        ShoppingCart shoppingCartCondition = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCartCondition);
        shoppingCartCondition.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> selectByCondition =shoppingCartMapper.selectByCondition(shoppingCartCondition);

       //2.如果待添加的商品已存在购物车，更新操作。
        if (selectByCondition!=null && selectByCondition.size()>0){

            ShoppingCart shoppingCart = selectByCondition.get(0);
            shoppingCart.setNumber(shoppingCart.getNumber()+1);
            shoppingCartMapper.update(shoppingCart);

            //3.如果待添加的商品不存在购物车，插入操作。
        }else {
            ShoppingCart shoppingCart = new ShoppingCart();
            //前端传递数据加入数据表
            BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);

            //判断插入的商品是菜品还是套餐
            if (shoppingCartDTO.getDishId()!=null){
                //插入的是菜品
                DishVO dishVO = dishMapper.selectById(shoppingCartDTO.getDishId());
                shoppingCart.setAmount(dishVO.getPrice());
                shoppingCart.setImage(dishVO.getImage());
                shoppingCart.setName(dishVO.getName());
            }else {
                //插入的是套餐
                Setmeal setmeal = setmealMapper.selectBySetmealId(shoppingCartDTO.getSetmealId());
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setName(setmeal.getName());
            }

            shoppingCart.setUserId(BaseContext.getCurrentId());
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCart.setNumber(1);


            shoppingCartMapper.insert(shoppingCart);

        }


    }

    /**
     * 删除购物车中的一个商品
     * @param shoppingCartDTO
     */
    @Override
    public void remove(ShoppingCartDTO shoppingCartDTO) {
        //1.把dto专为entity对象
        ShoppingCart shoppingCart = new ShoppingCart();
        //对象拷贝
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //2.查询购物车表，判读是否存在多个菜品/套餐
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByCondition(shoppingCart);
        ShoppingCart userShoppingCart = shoppingCartList.get(0);
        log.info("{}",userShoppingCart);
        if (!(userShoppingCart.getNumber()==1)){
            //2.1购物车菜品/套餐数量大于1，更新数据表，数量减一
            userShoppingCart.setNumber(userShoppingCart.getNumber()-1);
            shoppingCartMapper.update(userShoppingCart);

        }else {
            //2.2购物车菜品/套餐数量等于1，删除菜品
            shoppingCartMapper.deleteByShoppingCart(shoppingCart);
        }
    }

    /**
     * 查看购物车
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
        List<ShoppingCart> shoppingCartList =shoppingCartMapper.selectByUserId(BaseContext.getCurrentId());

        return shoppingCartList;

    }
}
