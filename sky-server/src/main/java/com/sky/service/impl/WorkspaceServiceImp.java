package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.entity.Orders;
import com.sky.entity.Setmeal;
import com.sky.mapper.*;
import com.sky.service.WorkspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class WorkspaceServiceImp implements WorkspaceService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    SetmealMapper setmealMapper;
    @Autowired
    DishMapper dishMapper;

    /**
     * 今日运营数据
     * @return
     */
    @Override
    public BusinessDataVO businessDate(LocalDate date) {
        BusinessDataVO businessDataVO = new BusinessDataVO();
        //1.新增用户数
        Integer newUsers=userMapper.getNewUserNumber(date);
        //2.订单完成率
        Double allNumber = Double.valueOf(orderMapper.getTodayNumberByStatus(null,date));
        Double completeNumber = Double.valueOf(orderMapper.getTodayNumberByStatus(Orders.COMPLETED,date));
        Double orderCompletionRate=0.0;
        if (allNumber!=0) {
            orderCompletionRate = completeNumber / allNumber;
        }
        //3.营业额
        Double money=orderMapper.getTurnover(Orders.COMPLETED,date);
        //4.平均客单价
        Double unitPrice=0.0;
        if (completeNumber!=0) {
             unitPrice = money / completeNumber;
        }
        //5.有效订单数
        businessDataVO.setNewUsers(newUsers);
        businessDataVO.setOrderCompletionRate(orderCompletionRate);
        businessDataVO.setTurnover(money);
        businessDataVO.setUnitPrice(unitPrice);
        businessDataVO.setValidOrderCount(completeNumber.intValue());
        return businessDataVO;
    }

    /**
     * 获取最近30天运营数据
     * @return
     */
    @Override
    public BusinessDataVO getBusinessDate(LocalDate begin,LocalDate end) {
        BusinessDataVO businessDataVO = new BusinessDataVO();
        //1.新增用户数
        Integer newUsers=userMapper.getRangeNewUserNumber(begin,end);
        //2.订单完成率
        Double allNumber = Double.valueOf(orderMapper.getRangeNumberByStatus(null,begin ,end));
        Double completeNumber = Double.valueOf(orderMapper.getRangeNumberByStatus(Orders.COMPLETED,begin,end));
        Double orderCompletionRate=0.0;
        if (allNumber!=0) {
            orderCompletionRate = completeNumber / allNumber;
        }
        //3.营业额
        Double money=orderMapper.getRangeTurnover(Orders.COMPLETED,begin,end);
        //4.平均客单价
        Double unitPrice=0.0;
        if (completeNumber!=0) {
            unitPrice = money / completeNumber;
        }
        //5.有效订单数
        businessDataVO.setNewUsers(newUsers);
        businessDataVO.setOrderCompletionRate(orderCompletionRate);
        businessDataVO.setTurnover(money);
        businessDataVO.setUnitPrice(unitPrice);
        businessDataVO.setValidOrderCount(completeNumber.intValue());
        return businessDataVO;
    }
    /**
     * 查询套餐总览
     * @return
     */
    @Override
    public SetmealOverViewVO overviewSetmeals() {
        SetmealOverViewVO setmealOverViewVO = new SetmealOverViewVO();
        //1.获取已停售套餐数量
        Integer discontinued=setmealMapper.getSetmealNumberByStatus(StatusConstant.DISABLE);
        //2.获取已起售套餐数量
        Integer sold=setmealMapper.getSetmealNumberByStatus(StatusConstant.ENABLE);
        setmealOverViewVO.setDiscontinued(discontinued);
        setmealOverViewVO.setSold(sold);
        return setmealOverViewVO;
    }
    /**
     * 查询菜品总览
     * @return
     */
    @Override
    public DishOverViewVO overviewDish() {
        DishOverViewVO dishOverViewVO = new DishOverViewVO();
        //1.获取已停售菜品
        Integer discontinued=dishMapper.getDishNumberByStatus(StatusConstant.DISABLE);
        //2.获取已起售菜品
        Integer sold=dishMapper.getDishNumberByStatus(StatusConstant.DISABLE);
        dishOverViewVO.setDiscontinued(discontinued);
        dishOverViewVO.setSold(sold);
        return dishOverViewVO;
    }

    /**
     * 查询订单管理数据
     * @return
     */
    @Override
    public OrderOverViewVO overviewOrder() {
        OrderOverViewVO orderOverViewVO = new OrderOverViewVO();
        //1.查询全部订单
        Integer allOrders = orderMapper.getNumberByStatus(null,null);
        //2.查询已取消数量
        Integer cancelledOrders = orderMapper.getNumberByStatus(Orders.CANCELLED,null);
        //3.查询已完成数量
        Integer completeOrder = orderMapper.getNumberByStatus(Orders.COMPLETED,null);
        //4.查询待派送数量
        Integer deliveredOrders = orderMapper.getNumberByStatus(Orders.CONFIRMED,null);
        //5.查询待接单数量
        Integer waitingOrders = orderMapper.getNumberByStatus(Orders.REFUND,null);
        orderOverViewVO.setAllOrders(allOrders);
        orderOverViewVO.setCancelledOrders(cancelledOrders);
        orderOverViewVO.setCompletedOrders(completeOrder);
        orderOverViewVO.setDeliveredOrders(deliveredOrders);
        orderOverViewVO.setWaitingOrders(waitingOrders);
        return orderOverViewVO;
    }
}
