package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserServiceImp implements UserService {

    @Autowired
    WeChatProperties weChatProperties;

    @Autowired
    UserMapper userMapper;

    @Override
    public User login(UserLoginDTO userLoginDTO) {
        //根据微信登录接口文档，和微信服务器进行交互，获取openid
        //请求方式式get
        //1.获取HttpClient工具类，实现与微信服务器建立请求
        Map<String,String> param=new HashMap<>();
//        属性	类型	必填	说明
//        appid	string	是	小程序 appId
//        secret	string	是	小程序 appSecret
//        js_code	string	是	登录时获取的 code，可通过wx.login获取
//        grant_type	string	是	授权类型，此处只需填写 authorization_code

        param.put("appid",weChatProperties.getAppid());
        param.put("secret",weChatProperties.getSecret());
        param.put("grantType",weChatProperties.getGrantType());
        param.put("js_code",userLoginDTO.getCode());
        String responseData = HttpClientUtil.doGet(weChatProperties.getWxLoginUrl(), param);
        //2.处理结果数据，解析出openid
        log.info("前端发送的code{}",userLoginDTO.getCode());
        log.info("返回的数据为:{}",responseData);
        JSONObject jsonObject = JSONObject.parseObject(responseData);
        String openid = jsonObject.getString("openid");
        //3.判读openid是否存在
        if (openid==null){
            //3.1封装异常对象交给全局异常处理器
            throw new LoginFailedException(MessageConstant.LOGIN_FAILED);
        }
        //3.1.如果存在，判断当前用户是否式第一次登录
        User user=userMapper.selectUserByOpenid(openid);
        if (user!=null){
            //4.1不是第一次登录
            return user;
        }
        //4.1是第一次登录，创建user对象插入user表中。
        User firstUser = User.builder()
                .openid(openid)
                .createTime(LocalDateTime.now())
                .build();
        userMapper.insertUser(firstUser);
        return firstUser;
    }
}
