package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorsMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealDishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.function.Consumer;

@Service
public class DishServiceImpl implements DishService {

    @Autowired
    DishMapper dishMapper;

    @Autowired
    DishFlavorsMapper dishFlavorsMapper;

    @Autowired
    SetmealDishMapper setmealDishMapper;


    /**
     * 添加菜品
     *
     * @param dishDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(DishDTO dishDTO) {
        Dish dish = new Dish();
        //对象拷贝
        BeanUtils.copyProperties(dishDTO, dish);
        //存入菜品
        dishMapper.insert(dish);
        //获取存入菜品id
        Long dishId = dish.getId();
        //获取菜品口味
        List<DishFlavor> flavors = dishDTO.getFlavors();
        //判读口味是否为空
        if (flavors != null && flavors.size() > 0) {
            //遍历菜品口味
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishId);
            });
            //向口味表添加n条数据
            dishFlavorsMapper.insertBatch(flavors);
        }
    }

    /**
     * 菜品分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO) {

        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());

        Dish dish = new Dish();

        dish.setName(dishPageQueryDTO.getName());
        dish.setStatus(dishPageQueryDTO.getStatus());

        Page<DishVO> dishPage=(Page<DishVO>) dishMapper.pageSelect(dish);

        return new PageResult(dishPage.getTotal(),dishPage.getResult());
    }

    /**
     * 菜品批量删除
     * @param ids
     */
    @Override
    @Transactional
    public void remove(List<Long> ids) {
        //1.查询菜品状态
        ids.forEach(id -> {
            DishVO dishVO = dishMapper.selectById(id);
            if (dishVO.getStatus()==1){
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }

        });
        //2.查询菜品是否关联套餐
        List<Long> setmealId=setmealDishMapper.selectByDishId(ids);
        if (setmealId!=null && setmealId.size()>0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //3.删除菜品
        dishMapper.deleteById(ids);
        //4.删除菜品口味
        ids.forEach(id -> {
            dishFlavorsMapper.delete(id);
        });
    }

    /**
     * 根据id查询菜品信息
     * @param id
     * @return
     */
    @Override
    public DishVO findById(Long id) {

        //查询菜品
        DishVO dishVO=dishMapper.selectById(id);
        //查新菜品口味
        List<DishFlavor> dishFlavors  =dishFlavorsMapper.selectById(id);

        dishVO.setFlavors(dishFlavors);
        return dishVO;
    }

    /**
     * 修改菜品
     * @param dishDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void change(DishDTO dishDTO) {
        Dish dish = new Dish();
        //对象拷贝
        BeanUtils.copyProperties(dishDTO,dish);
        //菜品修改
        dishMapper.update(dish);
        //菜品口味修改(删除+增加)
        Long dishId = dish.getId();
        dishFlavorsMapper.delete(dishId);

        //该菜品是否有口味
        if (dishDTO.getFlavors()!=null && dishDTO.getFlavors().size()>0){
            List<DishFlavor> flavors = dishDTO.getFlavors();
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishId);
            });

            dishFlavorsMapper.insertBatch(flavors);
        }

    }

    /**
     * 菜品起售/停售
     * @param status
     * @param id
     */
    @Override
    public void changeStatus(Integer status, Long id) {
        Dish dish = new Dish();
        dish.setStatus(status);
        dish.setId(id);
        dishMapper.update(dish);
    }

    /**
     * 管理端根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> findByCategoryId(Long categoryId) {
        List<Dish> dishList=dishMapper.selectDishByCategoryId(categoryId);
        return dishList;
    }
    /**
     * 用户端根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @Override
    public List<DishVO> userFindByCategoryId(Long categoryId) {
        //1.查询所有菜品
        List<DishVO> dishVOList=dishMapper.userSelectDishByCategoryId(categoryId);

        dishVOList.forEach(dishVO -> {
            //2.查询菜品口味
            List<DishFlavor> dishFlavors = dishFlavorsMapper.selectById(dishVO.getId());
            dishVO.setFlavors(dishFlavors);
        });
        return dishVOList;

    }

    /**
     * 用户端根据套餐id查询菜品
     * @param setmealId
     * @return
     */
    @Override
    public List<SetmealDishVO> getDishBySetmealId(Long setmealId) {
        List<SetmealDishVO> setMealDishVOList=dishMapper.selectDishBySermealId(setmealId);

        return setMealDishVOList;
    }
}
