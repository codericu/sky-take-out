package com.sky.service;

import com.sky.dto.*;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.vo.*;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {


    PageResult pageResult(OrdersPageQueryDTO ordersPageQueryDTO);


    OrderSubmitVO submitOrder(OrdersSubmitDTO ordersSubmitDTO);
    /**
     * 订单支付
     * @param ordersPaymentDTO
     * @return
     */

    OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception;

    /**
     * 支付成功，修改订单状态
     * @param outTradeNo
     */
    void paySuccess(String outTradeNo);

    void repetitionPayment(Long id) throws Exception;

    OrderVO orderDetailById(Long id);

    void cancel(OrdersCancelDTO ordersCancelDTO);

    /**
     * 订单数量统计
     * @return
     */
    OrderStatisticsVO orderCount();

    void complete(Long id);

    void rejectionOrder(OrdersRejectionDTO ordersRejectionDTO);

    void confirmOrder(OrdersConfirmDTO ordersConfirmDTO);

    void deliveryOrder(Long id);

    void reminder(Long id);



}
