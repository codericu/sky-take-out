package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {




    void save(SetmealDTO setmealDTO);

    PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO);

    SetmealVO findById(Long id);

    void changeSetmeal(SetmealDTO setmealDTO);

    void changeStatus(Integer status, Long id);

    void remove(List<Long> ids);

    List<Setmeal> userFindById(Long categoryId);
}
