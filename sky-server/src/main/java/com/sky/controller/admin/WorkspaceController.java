package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/admin/workspace")
@Api(tags = "工作台管理")
@Slf4j
public class WorkspaceController {

    @Autowired
    WorkspaceService workspaceService;

    /**
     * 查询今日运营数据
     * @return
     */
    @GetMapping("/businessData")
    public Result businessDate() {
        BusinessDataVO businessDataVO = workspaceService.businessDate(LocalDate.now());
        return Result.success(businessDataVO);
    }
    /**
     * 查询套餐总览
     * @return
     */
    @GetMapping("/overviewSetmeals")
    public Result overviewSetmeals() {
        SetmealOverViewVO setmealOverViewVO = workspaceService.overviewSetmeals();
        return Result.success(setmealOverViewVO);
    }
    /**
     * 查询菜品总览
     * @return
     */
    @GetMapping("/overviewDishes")
    public Result overviewDish() {
       DishOverViewVO dishOverViewVO =workspaceService.overviewDish();
        return Result.success(dishOverViewVO);
    }
    /**
     * 查询订单管理数据
     * @return
     */
    @GetMapping("/overviewOrders")
    public Result overviewOrder() {
        OrderOverViewVO orderOverViewVO =workspaceService.overviewOrder();
        return Result.success(orderOverViewVO);
    }


}
