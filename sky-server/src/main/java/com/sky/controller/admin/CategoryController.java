package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/category")
@Api(tags = "菜品分类管理")
public class CategoryController {

    @Autowired
    CategoryService categoryService;
    /**
     * 新增分类管理
     * @return
     */
    @PostMapping
    @ApiOperation("新增分类")
    public Result save(@RequestBody CategoryDTO categoryDTO){
        categoryService.save(categoryDTO);
        return Result.success();
    }

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分类分页查询")
    public Result pageQuery(CategoryPageQueryDTO categoryPageQueryDTO){
        PageResult pageQuery=categoryService.pageQuery(categoryPageQueryDTO);
        return Result.success(pageQuery);
    }

    /**
     * 通过id删除分类信息
     * @param id
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除分类")
    public Result remove(Long id){
        categoryService.remove(id);
        return Result.success();
    }

    /**
     * 通过id查询分类信息
     * @param id
     * @return
     */
    @GetMapping
    @ApiOperation("通过id查询分类信息")
    public Result findById(Long id){
        Category category=categoryService.findById(id);
        return Result.success(category);
    }

    /**
     * 修改分类信息
     * @return
     */
    @PutMapping()
    @ApiOperation("修改分类信息")
    public Result changeCategory(@RequestBody CategoryDTO categoryDTO){
        categoryService.change(categoryDTO);
        return Result.success();
    }
    /**
     * 修改分类状态
     * @param status
     * @param id
     */
    @PostMapping("/status/{status}")
    @ApiOperation("修改分类状态")
    public Result statusChange(@PathVariable Integer status,Long id){
        categoryService.statusChange(status,id);
        return Result.success();
    }

    /**
     * 通过类型查询
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("通过类型查询")
    public Result findByType(Integer type){
        Category[] categorys=categoryService.findByType(type);
        return Result.success(categorys);
    }
}
