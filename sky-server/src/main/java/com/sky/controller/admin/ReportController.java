package com.sky.controller.admin;

import com.sky.mapper.UserMapper;
import com.sky.result.Result;
import com.sky.service.OrderService;
import com.sky.service.ReportService;
import com.sky.service.UserService;
import com.sky.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@RestController
@RequestMapping("/admin/report")
@Api(tags = "数据统计管理")
@Slf4j
public class ReportController {


    @Autowired
    ReportService reportService;


    /**
     * 营业额统计接口
     * @param begin
     * @param end
     * @return
     */
    @ApiOperation("营业额统计")
    @GetMapping("/turnoverStatistics")
    public Result turnoverStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin ,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end){
        log.info("营业额统计执行");
        TurnoverReportVO turnoverReportVO =reportService.turnoverStatistics(begin,end);
        return Result.success(turnoverReportVO);
    }
    /**
     * 用户统计接口
     * @param begin
     * @param end
     * @return
     */
    @ApiOperation("用户统计")
    @GetMapping("/userStatistics")
    public Result userStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin ,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end){
        log.info("用户统计执行");
        UserReportVO userReportVO =reportService.userStatistics(begin,end);
        return Result.success(userReportVO);
    }
    /**
     * 订单统计接口
     * @param begin
     * @param end
     * @return
     */
    @ApiOperation("订单统计")
    @GetMapping("/ordersStatistics")
    public Result ordersStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin ,
                                 @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end){
        log.info("订单统计执行");
        OrderReportVO orderReportVO =reportService.ordersStatistics(begin,end);
        return Result.success(orderReportVO);
    }

    /**
     * 查询销量排行前十
     * @param begin
     * @param end
     * @return
     */
    @ApiOperation("查询销量排行前十")
    @GetMapping("/top10")
    public Result topTen(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin ,
                                   @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end){
        log.info("查询销量排行前十");
        SalesTop10ReportVO salesTop10ReportVO=reportService.topTen(begin,end);
        return Result.success(salesTop10ReportVO);
    }

    /**
     * 到处excel数据
     */
    @GetMapping("/export")
    public void report(HttpServletResponse response) throws IOException {

        ServletOutputStream outputStream = response.getOutputStream();
        reportService.exportDate(outputStream);
    }
}
