package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.mapper.DishFlavorsMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/admin/dish")
@Slf4j
@Api(tags = "菜品管理")
public class DishController {

    @Autowired
    DishService dishService;
    @Autowired
    RedisTemplate redisTemplate;



    /**
     * 添加菜品
     * @param dishDTO
     * @return
     */
    @PostMapping
    @ApiOperation("添加菜品")
    public Result save(@RequestBody DishDTO dishDTO){

        dishService.save(dishDTO);

        //清除缓存
        //1.清除dish下的所有缓存
        String delKeyPattern = "dish_" + dishDTO.getCategoryId();
        cleanDishRedis(delKeyPattern);
        //2.清除所有缓存

        return Result.success();
    }

    /**
     * 菜品分类查询
     * @param dishPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("菜品分类查询")
    public Result pageQuery(DishPageQueryDTO dishPageQueryDTO){
        PageResult pageQuery =dishService.pageQuery(dishPageQueryDTO);

        return Result.success(pageQuery);
    }

    /**
     * 批量删除菜品
     * @return
     */
    @DeleteMapping
    @ApiOperation("批量删除菜品")
    public Result remove(@RequestParam List<Long> ids){

        dishService.remove(ids);

        //清除缓存
        //1.清除dish下的所有缓存
        String delKeyPattern = "dish_*";
        //2.清除所有缓存
        cleanDishRedis(delKeyPattern);
        return Result.success();
    }

    /**
     * 菜品回显
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("菜品回显")
    public Result findById(@PathVariable Long id){
        DishVO dishVO =dishService.findById(id);
        return Result.success(dishVO);
    }

    /**
     * 修改菜品
     * @return
     */
    @PutMapping
    @ApiOperation("修改菜品")
    public Result change(@RequestBody DishDTO dishDTO){
        dishService.change(dishDTO);
        //清除缓存
        String delKeyPattern = "dish_" + dishDTO.getCategoryId();
        cleanDishRedis(delKeyPattern);
        return Result.success();
    }

    /**
     * 菜品起售/停售
     * @return
     */
    @PostMapping("status/{status}")
    @ApiOperation("菜品停售/停售")
    public Result changeStatus(@PathVariable Integer status ,Long id){
        dishService.changeStatus(status,id);
        //清除缓存
        //1.清除dish下的所有缓存
        String delKeyPattern = "dish_*";
        //2.清除所有缓存
        cleanDishRedis(delKeyPattern);
        return Result.success();
    }

    /**
     * 根据分类id查询菜品
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("根据分类id查询菜品")
    public Result findByCategoryId( Long categoryId){
        List<Dish> dishList=dishService.findByCategoryId(categoryId);
        return Result.success(dishList);
    }


    private void cleanDishRedis(String delKeyPattern){
        //1.清除dish下的所有缓存
        Set keys = redisTemplate.keys(delKeyPattern);
        redisTemplate.delete(keys);
        //2.清除所有缓存
    }
}
