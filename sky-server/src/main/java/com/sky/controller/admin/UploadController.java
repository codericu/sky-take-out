package com.sky.controller.admin;

import com.sky.annotaion.AutoFill;
import com.sky.constant.MessageConstant;
import com.sky.properties.AliOssProperties;
import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传
 */
@RestController
@Slf4j
@Api(tags = "通用接口")
public class UploadController {

    @Autowired
    AliOssUtil aliOssUtil;


    @PostMapping("/admin/common/upload")
    @ApiOperation("文件上传")
    public Result upload(@RequestBody MultipartFile file) throws IOException {

        log.info("文件名{}",file.getName());


        try {
            //1.获取原始文件名
            String originalFilename = file.getOriginalFilename();
            //2.获取文件后缀
            String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
            //3.生成唯一文件名
            String objectName = UUID.randomUUID().toString() + extension;
            //4.上传阿里云
            String url = aliOssUtil.upload(file.getBytes(), objectName);
            log.info("{}",url);
            return Result.success(url);
        } catch (IOException e) {
           log.error("文件上传失败{}",e);
        }


        return Result.success(MessageConstant.UPLOAD_FAILED);
    }
}
