package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/setmeal")
@Api(tags = "套餐管理")
public class SetmealController {
    @Autowired
    SetmealService setmealService;


    /**
     * 添加套餐
     * @return
     */
    @PostMapping
    @ApiOperation("添加套餐")
    @CacheEvict(value = "setmealCache" ,key = "'setmeal_'+#setmealDTO.categoryId",allEntries=true)
    public Result save(@RequestBody SetmealDTO setmealDTO){
        setmealService.save(setmealDTO);
        return Result.success();
    }

    /**
     * 套餐分页查询
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询")
    public Result pageQuery(SetmealPageQueryDTO setmealPageQueryDTO){
        PageResult pageQuery =setmealService.pageQuery(setmealPageQueryDTO);
        return Result.success(pageQuery);
    }

    /**
     * 根据id查询套餐
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据id查询套餐")
    public Result findById(@PathVariable Long id){
        SetmealVO setmealVO =setmealService.findById(id);
        return Result.success(setmealVO);
    }

    /**
     * 套餐更新
     * @return
     */
    @PutMapping
    @ApiOperation("更新套餐")
    public Result changeSetmeal(@RequestBody SetmealDTO setmealDTO){
        setmealService.changeSetmeal(setmealDTO);
        return Result.success();
    }

    /**
     * 套餐起售、停售
     * @return
     */
    @PostMapping("/status/{status}")
    public Result changeStatus(@PathVariable Integer status ,Long id){
        setmealService.changeStatus(status,id);
        return Result.success();
    }
    @DeleteMapping
    public Result remove(@RequestParam List<Long> ids){
        setmealService.remove(ids);
        return Result.success();
    }

}
