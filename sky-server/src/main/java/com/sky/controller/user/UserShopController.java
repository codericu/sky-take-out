package com.sky.controller.user;

import com.sky.constant.ShopConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/shop")
@Api(tags = "用户店铺控制")
@Slf4j
public class UserShopController {

    @Autowired
    RedisTemplate redisTemplate;



    /**
     * 获取店铺状态
     * @return
     */
    @GetMapping("/status")
    @ApiOperation("获取店铺状态")
    public Result getShopStatus(){
        Integer status = (Integer) redisTemplate.opsForValue().get(ShopConstant.SHOP_STATUS);
        log.info("店铺状态为：{}",status);
        return Result.success(status);
    }

}
