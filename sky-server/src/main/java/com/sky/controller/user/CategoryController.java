package com.sky.controller.user;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.mapper.CategoryMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import static com.sun.tools.doclint.Entity.and;

@RestController("userCategoryController")
@RequestMapping("/user/category")
@Api(tags = "C端——菜品分类管理")
public class CategoryController {


    @Autowired
    private CategoryService categoryService;

    /**
     * 通过类型查询
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("通过类型查询")
    public Result findByType(Integer type){


        Category[] categorys=categoryService.findByType(type);

        return Result.success(categorys);

    }


}
