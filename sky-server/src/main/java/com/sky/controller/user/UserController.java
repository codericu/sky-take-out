package com.sky.controller.user;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.properties.JwtProperties;
import com.sky.properties.WeChatProperties;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.service.impl.UserServiceImp;
import com.sky.utils.JwtUtil;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user/user")
@Api(tags = "用户小程序端管理")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    JwtProperties jwtProperties;


    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Result login(@RequestBody UserLoginDTO userLoginDTO) {
        //1.接受传递参数
        //2.调用业务层实现用户登录
        User user =userService.login(userLoginDTO);
        //3.生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        //3.1自定义载荷
        claims.put(JwtClaimsConstant.USER_ID,user.getId());

        String jwt = JwtUtil.createJWT(
         jwtProperties.getUserSecretKey(),
         jwtProperties.getUserTtl(),
         claims
        );
        //4.封装vo对象
        UserLoginVO userLoginVO = UserLoginVO.builder()
                .openid(user.getOpenid())
                .token(jwt)
                .id(user.getId())
                .build();

        return Result.success(userLoginVO);
    }
    /**
     * 用户等出
     */
    @PostMapping("/logout")
    public Result logout(){
        return Result.success();
    }
}
