package com.sky.controller.user;

import com.sky.dto.*;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;


import com.sky.service.OrderService;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "用户订单管理")
@Slf4j
@RestController("userOrderController")
@RequestMapping("/user/order")
public class OrderController {


    @Autowired
    OrderService orderService;

    /**
     * 提交订单
     */
    @PostMapping("/submit")
    @ApiOperation("提交订单")
    public Result submitOrder(@RequestBody OrdersSubmitDTO ordersSubmitDTO){

        OrderSubmitVO orderSubmitVO=orderService.submitOrder(ordersSubmitDTO);
        return Result.success(orderSubmitVO);

    }

    /**
     * 订单支付管理
     * @return
     */
    @PutMapping("/payment")
    @ApiOperation("订单支付管理")
    public Result payment(@RequestBody OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        log.info("订单支付：{}",ordersPaymentDTO);
        OrderPaymentVO ordersPaymentVO=orderService.payment(ordersPaymentDTO);
        log.info("生成预付款交易单:{}",ordersPaymentVO);
        return Result.success(ordersPaymentDTO);
    }

    /**
     * 历史订单查询
     * @return
     */
    @GetMapping("/historyOrders")
    @ApiOperation("历史订单查询")
    public Result historyOrders(OrdersPageQueryDTO ordersPageQueryDTO){

       PageResult pageResult =orderService.pageResult(ordersPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 再来一单
     */
    @PostMapping("/repetition/{id}")
    public Result repetition(@PathVariable Long id) throws Exception {
        orderService.repetitionPayment(id);
        return Result.success();
    }

    /**
     * 查询订单详情
     * @return
     */
    @GetMapping("/orderDetail/{id}")
    public Result orderDetail(@PathVariable("id") Long id){
        OrderVO orderVO= orderService.orderDetailById(id);
        return Result.success(orderVO);
    }

    /**
     * 取消订单
     * @return
     */
    @PutMapping("cancel/{id}")
    public Result cancel(@PathVariable("id") Long id){
        OrdersCancelDTO ordersCancelDTO = new OrdersCancelDTO();
        ordersCancelDTO.setId(id);
        orderService.cancel(ordersCancelDTO);
        return Result.success();
    }

    /**
     * 用户催单管理
     * @return
     */
    @GetMapping("/reminder/{id}")
    public Result reminder(@PathVariable Long id){
        log.info("用户催单管理执行,订单id为：{}",id);
        orderService.reminder(id);
        return Result.success();
    }
}
