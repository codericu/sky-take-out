package com.sky.controller.user;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealDishVO;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("userSetmealController")
@RequestMapping("/user/setmeal")
@Api(tags = "套餐管理")
public class SetmealController {
    @Autowired
    SetmealService setmealService;

    @Autowired
    DishService dishService;




    /**
     * 根据id查询套餐
     * @return
     */
    @Cacheable(value = "setmealCache",key = "'setmeal_'+#categoryId")
    @GetMapping("/list")
    @ApiOperation("根据id查询套餐")
    public Result findById( Long categoryId){

        List<Setmeal> setmeal=setmealService.userFindById(categoryId);

        return Result.success(setmeal);
    }
    /**
     * 根据套餐id查询菜品
     * @return
     */
    @GetMapping("/dish/{id}")
    @ApiOperation("根据套餐id查询菜品")
    public Result findByCategoryId(@PathVariable Long id){
        List<SetmealDishVO> setMealDishVO =dishService.getDishBySetmealId(id);

        return Result.success(setMealDishVO);
    }



}
