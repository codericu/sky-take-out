package com.sky.aspect;

import com.sky.annotaion.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Component
@Aspect
@Slf4j
public class AutoFillAspect {


    /**
     * 切入点
     */
    @Pointcut("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.annotaion.AutoFill)")
    public void autoFillPointCut(){}
    @Pointcut("@annotation(com.sky.annotaion.AutoFill)")
    public void autoFill(){};

    /**
     * 前置通知，在通知中进行公共字段的赋值
     */
//    @Before("autoFillPointCut()")
    public void autoFill(JoinPoint joinPoint){
        log.info("开始进行公共字段自动填充...");

        //获取到当前被拦截的方法上的数据库操作类型
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();//方法签名对象
        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);//获得方法上的注解对象
        OperationType operationType = autoFill.value();//获得数据库操作类型

        //获取到当前被拦截的方法的参数--实体对象
        Object[] args = joinPoint.getArgs();
        if(args == null || args.length == 0){
            return;
        }

        Object entity = args[0];

        //准备赋值的数据
        LocalDateTime now = LocalDateTime.now();
        Long currentId = BaseContext.getCurrentId();

        //根据当前不同的操作类型，为对应的属性通过反射来赋值
        if(operationType == OperationType.INSERT){
            //为4个公共字段赋值
            try {
                Method setCreateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
                Method setCreateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
                Method setUpdateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

                //通过反射为对象属性赋值
                setCreateTime.invoke(entity,now);
                setCreateUser.invoke(entity,currentId);
                setUpdateTime.invoke(entity,now);
                setUpdateUser.invoke(entity,currentId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(operationType == OperationType.UPDATE){
            //为2个公共字段赋值
            try {
                Method setUpdateTime = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

                //通过反射为对象属性赋值
                setUpdateTime.invoke(entity,now);
                setUpdateUser.invoke(entity,currentId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Around("autoFill()")
    public Object autoFill(ProceedingJoinPoint pjp) throws Throwable {
        log.info("公共字段自动填充");
        //1,判断当前用户操作的类型；insert/update
        //获取目标骄傲方法对象，获取注解对象中的value属性，判断操作类型：通过来连接点对象获取目标方法对象
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        //获取目标方法
        Method method = signature.getMethod();
        //获取目标对象方法上的注解对象
        AutoFill annotation = method.getAnnotation(AutoFill.class);
        //获取注解对象属性
        OperationType value = annotation.value();
        //2.获取目标参数实体对象
        Object[] args = pjp.getArgs();
        Object arg = args[0];
        //获取当前实体对象的字节码文件
        Class<?> methodCls = arg.getClass();
        //根据注解属性分别操作
        if (value==OperationType.INSERT){
            //使用反射的方式，设置属性值：通过方法
            //获取set方法
            Method setCreateTime = methodCls.getDeclaredMethod("setCreateTime", LocalDateTime.class);
            Method setUpdateTime = methodCls.getDeclaredMethod("setUpdateTime", LocalDateTime.class);
            Method setCreateUser = methodCls.getDeclaredMethod("setCreateUser", Long.class);
            Method setUpdateUser = methodCls.getDeclaredMethod("setUpdateUser", Long.class);
            //执行set方法
            Object setCreatTimeObj = setCreateTime.invoke(arg, LocalDateTime.now());
            Object setUpdateTimeObj = setUpdateTime.invoke(arg, LocalDateTime.now());
            Object setCreatUserObj = setCreateUser.invoke(arg, BaseContext.getCurrentId());
            Object setUpdateUserObj = setUpdateUser.invoke(arg, BaseContext.getCurrentId());
        }else {
//            使用反射的方式，设置属性值：通过属性
            Field updateTime = methodCls.getDeclaredField("updateTime");
            Field updateUser = methodCls.getDeclaredField("updateUser");
            //设置允许暴力破解
            updateTime.setAccessible(true);
            updateUser.setAccessible(true);
            //通过反射设置属性
            updateTime.set(arg,LocalDateTime.now());
            updateUser.set(arg,BaseContext.getCurrentId());

        }
        return pjp.proceed();
    }
}
